package main

/*
	NOT WORKING in WINDOWS
 */


import (
	"flag"
	"fmt"
	"net"
	"net/http"
	"os"
	"os/exec"
	"syscall"
	"time"
	"log"
)

var FD *int = flag.Int("fd", 0, "Server socket FD")
var PID int = syscall.Getpid()
var listener1 net.Listener
var file1 *os.File = nil
var exit1 chan int = make(chan int)
var stop1 = false

func main() {
	fo1, err := os.Create(fmt.Sprintf("pid-%d.log", PID))
	if err != nil { panic(err) }
	log.SetOutput(fo1)
	log.Println("Grace1 ", PID)

	flag.Parse()

	s := &http.Server{Addr: ":7777",
		ReadTimeout:  10 * time.Second,
		WriteTimeout: 10 * time.Second,
	}

	http.HandleFunc("/", DefHandler)
	http.HandleFunc("/stop", StopHandler)
	http.HandleFunc("/restart", RestartHandler)
	http.HandleFunc("/think", ThinkHandler)


	if *FD != 0 {
		log.Println("Starting with FD ", *FD)
		file1 = os.NewFile(uintptr(*FD), "parent socket")
		listener1, err = net.FileListener(file1)
		if err != nil {
			log.Fatalln("fd listener failed: ", err)
		}
	} else {
		log.Println("Virgin Start")
		listener1, err = net.Listen("tcp", s.Addr)
		if err != nil {
			log.Fatalln("listener failed: ", err)
		}
	}

	err = s.Serve(listener1)
	log.Println("EXITING", PID)
	<-exit1
	log.Println("EXIT", PID)

}

func DefHandler(w http.ResponseWriter, req *http.Request) {
	fmt.Fprintf(w, "def handler %d %s", PID, time.Now().String())
}

func ThinkHandler(w http.ResponseWriter, req *http.Request) {
	time.Sleep(5 * time.Second)
	fmt.Fprintf(w, "think handler %d %s", PID, time.Now().String())
}

func StopHandler(w http.ResponseWriter, req *http.Request) {
	log.Println("StopHandler", req.Method)
	if(stop1){
		fmt.Fprintf(w, "stopped %d %s", PID, time.Now().String())
	}
	stop1 = true
	fmt.Fprintf(w, "stop %d %s", PID, time.Now().String())
	go func() {
		listener1.Close()
		if file1 != nil {
			file1.Close()
		}

		exit1<-1
	}()
}

func RestartHandler(w http.ResponseWriter, req *http.Request) {
	log.Println("RestartHandler", req.Method)
	if(stop1){
		fmt.Fprintf(w, "stopped %d %s", PID, time.Now().String())
	}
	stop1 = true
	fmt.Fprintf(w, "restart %d %s", PID, time.Now().String())

	go func() {
		listener1.Close()
		if file1 != nil {
			file1.Close()
		}

		cmd := exec.Command("./grace1")
		err := cmd.Start()
		if err != nil {
			log.Fatalln("starting error:", err)
		}
		exit1<-1
	}()
}