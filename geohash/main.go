package main

import (
	"github.com/mmcloughlin/geohash"
	"log"
)

func main() {

	var lat = 48.669
	var lng = -14.329
	var lib = geohash.EncodeWithPrecision(lat, lng, 5) // 1-9 level
	log.Println(lib)

}