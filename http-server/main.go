package main

import (
	"flag"
	gh "github.com/gorilla/handlers"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strings"
)

const (
	// Mount point where static content is served, http://host-name/<defaultStaticMount>
	defaultStaticMount = "/"

	// Local path to static content
	defaultStaticPath = "static"
)

func main() {

	executable, _ := os.Executable()
	rootpath, _ := filepath.Split(executable)

	var addr = flag.String("addr", ":8080", "http service address")
	var staticPath = flag.String("static_data", defaultStaticPath, "Path to directory with static files to be served.")
	flag.Parse()

	mux := http.NewServeMux()

	var staticMountPoint string
	if *staticPath != "" && *staticPath != "-" {
		// Resolve path to static content.
		*staticPath = toAbsolutePath(rootpath, *staticPath)

		staticMountPoint = "/"
		if staticMountPoint == "" {
			staticMountPoint = defaultStaticMount
		} else {
			if !strings.HasPrefix(staticMountPoint, "/") {
				staticMountPoint = "/" + staticMountPoint
			}
			if !strings.HasSuffix(staticMountPoint, "/") {
				staticMountPoint += "/"
			}
		}
		mux.Handle(staticMountPoint,
			// Add optional Cache-Control header
			cacheControlHandler(39600,
				// Optionally add Strict-Transport_security to the response
				hstsHandler(
					// Add gzip compression
					gh.CompressHandler(
						// And add custom formatter of errors.
						httpErrorHandler(
							// Remove mount point prefix
							http.StripPrefix(staticMountPoint,
								http.FileServer(http.Dir(*staticPath))))))))
		log.Printf("Serving static content from '%s' at '%s'", *staticPath, staticMountPoint)
	} else {
		log.Println("Static content is disabled")
	}

	mux.HandleFunc("/ws", func(w http.ResponseWriter, r *http.Request) {
		// handle ws
	})
	err := listenAndServe(*addr, mux, nil, signalHandler())

	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}

func toAbsolutePath(base, path string) string {
	if filepath.IsAbs(path) {
		return path
	}
	return filepath.Clean(filepath.Join(base, path))
}
