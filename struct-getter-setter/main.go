package main

import (
	"encoding/json"
	"log"
	"time"
)

var globals struct {
	stats *Stats
}

func main() {

	globals.stats = newStats()

	log.Printf("RESULT TOTAL SESSIONS: %+v", globals.stats.totalSessions)
	log.Printf("RESULT LIVE TOPICS: %+v", globals.stats.liveTopics)
	log.Printf("RESULT ONLINE USERS: %+v", globals.stats.onlineUsersByGroup)
	for k, v := range globals.stats.userMessagesByGroup {
		log.Printf("RESULT USER MESSAGES: Key: %s Object: %+v", k, v.userMessages)
	}

	log.Println("------------------------------------------------------------------------")

	globals.stats.channels.totalSessions <- map[string]interface{}{"value": int64(345)}
	globals.stats.channels.liveTopics <- map[string]interface{}{"value": int64(1)}
	//globals.stats.channels.liveTopics <- map[string]interface{}{"value": int64(1)}
	//globals.stats.channels.liveTopics <- map[string]interface{}{"value": int64(1)}
	globals.stats.channels.onlineUsersByGroup <- map[string]interface{}{"groupName": "aaa", "value": int64(1)}
	globals.stats.channels.usersMessagesByGroup <- map[string]interface{}{"groupName": "bbb", "userName": "eshmat", "value": int64(1)}
	//globals.stats.channels.onlineUsersByGroup <- map[string]interface{}{"groupName": "aaa", "value": int64(1)}
	globals.stats.channels.usersMessagesByGroup <- map[string]interface{}{"groupName": "bbb", "userName": "eshmat", "value": int64(1)}
	//globals.stats.channels.onlineUsersByGroup <- map[string]interface{}{"groupName": "aaa", "value": int64(1)}
	globals.stats.channels.usersMessagesByGroup <- map[string]interface{}{"groupName": "bbb", "userName": "eshmat", "value": int64(1)}

	time.Sleep(time.Second)
	log.Printf("RESULT TOTAL SESSIONS: %+v", globals.stats.totalSessions)
	log.Printf("RESULT LIVE TOPICS: %+v", globals.stats.liveTopics)
	log.Printf("RESULT ONLINE USERS: %+v", globals.stats.onlineUsersByGroup)
	for k, v := range globals.stats.userMessagesByGroup {
		log.Printf("RESULT USER MESSAGES: Key: %s Object: %+v", k, v.userMessages)
	}

	log.Println("------------------------------------------------------------------------")

	out, _ := json.Marshal(globals.stats)
	log.Println(out)
	globals.stats.channels.totalSessions <- map[string]interface{}{"value": int64(400)}
	globals.stats.channels.liveTopics <- map[string]interface{}{"value": int64(-1)}
	globals.stats.channels.onlineUsersByGroup <- map[string]interface{}{"groupName": "aaa", "value": int64(-1)}
	globals.stats.channels.usersMessagesByGroup <- map[string]interface{}{"groupName": "bbb", "userName": "eshmat", "value": int64(1)}
	globals.stats.channels.usersMessagesByGroup <- map[string]interface{}{"groupName": "bbb", "userName": "toshmat", "value": int64(1)}

	time.Sleep(time.Second)
	log.Printf("RESULT TOTAL SESSIONS: %+v", globals.stats.totalSessions)
	log.Printf("RESULT LIVE TOPICS: %+v", globals.stats.liveTopics)
	log.Printf("RESULT ONLINE USERS: %+v", globals.stats.onlineUsersByGroup)
	for k, v := range globals.stats.userMessagesByGroup {
		log.Printf("RESULT USER MESSAGES: Key: %s Object: %+v", k, v.userMessages)
	}

	time.Sleep(time.Minute)

}
