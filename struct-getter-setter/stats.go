package main

import (
	"log"
	"sync/atomic"
	"time"
)

type StatsChannels struct {
	totalTopics          chan map[string]interface{}
	liveTopics           chan map[string]interface{}
	totalSessions        chan map[string]interface{}
	liveSessions         chan map[string]interface{}
	onlineUsersByGroup   chan map[string]interface{}
	usersMessagesByGroup chan map[string]interface{}
	messagesByTypes      chan map[string]interface{}
}

type UserMessages struct {
	userMessages map[string]int64
}

type Stats struct {
	channels            *StatsChannels
	totalTopics         int64
	liveTopics          int64
	totalSessions       int64
	liveSessions        int64
	onlineUsersByGroup  map[string]int64
	userMessagesByGroup map[string]*UserMessages
	messagesTypes       map[string]int64
}

func (st *Stats) IncTotalTopics(value int64) {
	atomic.AddInt64(&st.totalTopics, value)

}
func (st *Stats) IncLiveTopics(value int64) {
	atomic.AddInt64(&st.liveTopics, value)

}
func (st *Stats) DecLiveTopics(value int64) {
	if value > st.liveTopics {
		st.liveTopics = 0
	} else {
		st.liveTopics += value
	}

}
func (st *Stats) IncTotalSession(value int64) {
	atomic.AddInt64(&st.totalSessions, value)

}
func (st *Stats) SetLiveSession(value int64) {
	atomic.StoreInt64(&st.liveSessions, value)

}

func (st *Stats) IncOnlineUsersByGroup(groupName string, value int64) {
	objMap := st.onlineUsersByGroup
	if valueObj, ok := objMap[groupName]; !ok {
		objMap[groupName] = value
	} else {
		objMap[groupName] = valueObj + value
	}
}
func (st *Stats) DecOnlineUsersByGroup(groupName string, value int64) {
	objMap := st.onlineUsersByGroup
	if valObj, ok := objMap[groupName]; !ok {
	} else {
		if value > valObj {
			objMap[groupName] = 0
		} else {
			objMap[groupName] = valObj + value
		}
	}
}
func (st *Stats) SetOnlineUsersByGroup(groupName string, value int64) {
	objMap := st.onlineUsersByGroup
	objMap[groupName] = value
}

func (st *Stats) IncUserMessagesByGroup(groupName string, userName string, value int64) {
	objGroupsMap := st.userMessagesByGroup
	if userMessages, ok := objGroupsMap[groupName]; !ok {
		userMessages := &UserMessages{userMessages: map[string]int64{userName: value}}
		st.userMessagesByGroup[groupName] = userMessages
	} else {
		objUsersMap := userMessages.userMessages
		if valueObj, ok := objUsersMap[userName]; !ok {
			objUsersMap[userName] = value
		} else {
			objUsersMap[userName] = valueObj + value
		}
	}
}
func (st *Stats) DecUserMessagesByGroup(groupName string, userName string, value int64) {
	objGroupsMap := st.userMessagesByGroup
	if userMessages, ok := objGroupsMap[groupName]; !ok {
	} else {
		objUsersMap := userMessages.userMessages
		if valueObj, ok := objUsersMap[userName]; !ok {
		} else {
			if value > valueObj {
				objUsersMap[groupName] = 0
			} else {
				objUsersMap[groupName] = valueObj + value
			}
		}
	}
}
func (st *Stats) SetUserMessagesByGroup(groupName string, userName string, value int64) {
	objGroupsMap := st.userMessagesByGroup
	if userMessages, ok := objGroupsMap[groupName]; !ok {
		userMessages := &UserMessages{userMessages: map[string]int64{userName: value}}
		st.userMessagesByGroup[groupName] = userMessages
	} else {
		objUsersMap := userMessages.userMessages
		objUsersMap[userName] = value
	}
}

func (st *Stats) IncMessageTypes(messageType string, value int64) {
	objMap := st.messagesTypes
	if valueObj, ok := objMap[messageType]; !ok {
		objMap[messageType] = value
	} else {
		objMap[messageType] = valueObj + value
	}
}
func (st *Stats) DecMessageTypes(messageType string, value int64) {
	objMap := st.messagesTypes
	if valObj, ok := objMap[messageType]; !ok {
	} else {
		if value > valObj {
			objMap[messageType] = 0
		} else {
			objMap[messageType] = valObj + value
		}
	}
}
func (st *Stats) SetMessageTypes(messageType string, value int64) {
	objMap := st.messagesTypes
	objMap[messageType] = value
}

func newStats() *Stats {

	var statsChannels = &StatsChannels{
		totalTopics:          make(chan map[string]interface{}, 1024),
		liveTopics:           make(chan map[string]interface{}, 1024),
		totalSessions:        make(chan map[string]interface{}, 1024),
		liveSessions:         make(chan map[string]interface{}, 1024),
		onlineUsersByGroup:   make(chan map[string]interface{}, 1024),
		usersMessagesByGroup: make(chan map[string]interface{}, 1024),
		messagesByTypes:      make(chan map[string]interface{}, 1024),
	}

	var stats = &Stats{
		channels:            statsChannels,
		totalTopics:         0,
		liveTopics:          0,
		totalSessions:       0,
		liveSessions:        0,
		onlineUsersByGroup:  make(map[string]int64),
		userMessagesByGroup: make(map[string]*UserMessages),
		messagesTypes:       make(map[string]int64),
	}

	go stats.collect()

	log.Printf("Stats initialized.")

	return stats

}

func (st *Stats) collect() {

	sendTimer := time.NewTimer(time.Second * 10)
	sendTimer.Stop()

	for {

		select {

		case obj := <-st.channels.totalTopics:
			var value = obj["value"].(int64)
			st.IncTotalTopics(value)

		case obj := <-st.channels.liveTopics:
			var value = obj["value"].(int64)
			if value >= 0 {
				st.IncLiveTopics(value)
			} else {
				st.DecLiveTopics(value)
			}

		case obj := <-st.channels.totalSessions:
			var value = obj["value"].(int64)
			st.IncTotalSession(value)

		case obj := <-st.channels.liveSessions:
			var value = obj["value"].(int64)
			st.SetLiveSession(value)

		case obj := <-st.channels.onlineUsersByGroup:
			var groupName = obj["groupName"].(string)
			var value = obj["value"].(int64)
			if value >= 0 {
				st.IncOnlineUsersByGroup(groupName, value)
			} else {
				st.DecOnlineUsersByGroup(groupName, value)
			}

		case obj := <-st.channels.usersMessagesByGroup:
			var groupName = obj["groupName"].(string)
			var userName = obj["userName"].(string)
			var value = obj["value"].(int64)
			st.IncUserMessagesByGroup(groupName, userName, value)

		case obj := <-st.channels.messagesByTypes:
			var msgType = obj["msgType"].(string)
			var value = obj["value"].(int64)
			if value >= 0 {
				st.IncMessageTypes(msgType, value)
			} else {
				st.DecMessageTypes(msgType, value)
			}

		case <-sendTimer.C:
			// Publish stats to queue (every 5 second)
			if st == nil {
				continue
			}
			log.Println("I push DATA to EXPORTER !!!!!!!!!!")
			sendTimer.Reset(time.Second)
		}

	}

}