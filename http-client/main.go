package http_client

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"regexp"
	"strconv"
)

func main() {

	values := url.Values{}
	values.Set("lengths_age", strconv.Itoa(2))

	req, err := http.NewRequest("GET", "http://localhost:4545/api/overview?"+values.Encode(), nil)
	if err != nil {
		log.Println(err)
	}

	req.SetBasicAuth("admin", "web@1234")

	client := &http.Client{}
	response, err := client.Do(req)
	if err != nil {
		log.Println(err)
	}

	// Close read
	defer func(response *http.Response) {
		err := response.Body.Close()
		if err != nil {
			fmt.Println(err)
		}
	}(response)

	// Convert to bytes
	bytes, err := ioutil.ReadAll(response.Body)
	if err != nil {
		log.Println(err)
	}

	// Fix JSON
	regex := regexp.MustCompile(`"socket_opts":\[]`)
	s := regex.ReplaceAllString(string(bytes), `"socket_opts":{}`)

	bytes = []byte(s)

	// Unmarshal JSON
	var resp Response
	err = json.Unmarshal(bytes, &resp)
	if err != nil {
		log.Println(err)
	}

	log.Println("OK")

}

type Response struct {
	body string
}
