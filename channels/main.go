package main

import "fmt"

func main() {

	Simple()
	ReturnResult()
	WithBuffer()
	CloseChannel()

}

/*
Каналы (channels) представляют инструменты коммуникации между горутинами. Для определения канала применяется ключевое слово chan:
chan тип_элемента
После слова chan указывается тип данных, которые будут передаться с помощью канала. Например:
var intCh chan int
Здесь переменная intCh представляет представляет канал, который передает данные типа int.
Для передачи данных в канал или, наоборот, из канала применяется операция <- (направленная влево стрелка). Например, передача данных в канал:
intCh <- 5
В данном случае в канал посылается число 5. Получение данных из канала в переменную:
val := <- intCh
Если ранее в канал было отправлено число 5, то при выполнении операции <- intCh мы можем получить это число в переменную val.
Стоит учитывать, что мы можем отправить в канал и получить из канала данные только того типа, который представляет канал.
Так, в примере с каналом intCh это данные типа int.
Как правило, отправителем данных является одна горутина, а получателем - другая горутина.
При простом определении переменной канала она имеет значение nil, то есть по сути канал неинициализирован. Для инициализации применяется функция make().
В зависимости от определения емкости канала он может быть буферизированным или небуферизированным.
Небуфферизированные каналы
Для создания небуферизированного канала вызывается функция make() без указания емкости канала:
var intCh chan int = make(chan int) // канал для данных типа int
strCh := make(chan string)  // канал для данных типа string
Если канал пустой, то горутина-получатель блокируется, пока в канале не окажутся данные. Когда горутина-отправитель посылает данные,
горутина-получатель получает эти данные и возобновляет работу.
Горутина-отправитель может отправлять данные только в пустой канал. Горутина-отправитель блокируется до тех пор, пока данные из канала не будут получены.

Например: Simple()
1) Запускается функция main. Она создает канал intCh и запускает горутину в виде анонимной функции.
2) Функция main продолжает выполняться и блокируется на строке fmt.Println(<-intCh), пока не будут получены данные.
3) Параллельно выполняется запущенная горутина в виде анонимной функции. В конце своего выполнения она отправляет даные через канал: intCh <- 5. Горутина блокируется, пока функция main не получит данные.
4) Функция main получает отправленные данные, деблокируется и продолжает свою работу.

 */
func Simple() {

	intCh := make(chan int)

	go func() {
		fmt.Println("Go routine starts")
		intCh <- 5 // блокировка, пока данные не будут получены функцией main
	}()
	fmt.Println(<-intCh) // получение данных из канала
	fmt.Println("The End")
}


/*
В данном случае горутина определена в виде анонимной функции и поэтому она имеет доступ к окружению, в том числе к переменной intCh.
Если же мы работаем с обычными функциями, то объекты каналов надо передавать через параметры:
 */
func ReturnResult() {

	intCh := make(chan int)

	go factorial(5, intCh)  // вызов горутины
	fmt.Println(<-intCh) // получение данных из канала
	fmt.Println("The End")
}

func factorial(n int, ch chan int){

	result := 1
	for i := 1; i <= n; i++{
		result *= i
	}
	fmt.Println(n, "-", result)

	ch <- result     // отправка данных в канал
}



/*
Буферизированные каналы
Буферизированные каналы также создаются с помощью функции make(), только в качестве второго аргумента в функцию передается емкость канала.
Если канал пуст, то получатель ждет, пока в канале появится хотя бы один элемент.
При отправке данных горутина-отправитель ожидает, пока в канале не освободится место для еще одного элемента и отправляет элемент,
только тогда, когда в канале освобождается для него место.
	intCh := make(chan int, 3)
    intCh <- 10
    intCh <- 3
    intCh <- 24
    fmt.Println(<-intCh)     // 10
    fmt.Println(<-intCh)     // 3
    fmt.Println(<-intCh)     //24
 */

func WithBuffer() {

	intCh := make(chan int, 3)
	intCh <- 10
	intCh <- 3
	intCh <- 24
	intCh <- 15  // блокировка - функция main ждет, когда освободится место в канале

	fmt.Println(<-intCh)

	// С помощью встроенных функций cap() и len() можно получить соответственно емкость и количество элементов в канале:
	intCh_2 := make(chan int, 3)
	intCh_2 <- 10
	fmt.Println(cap(intCh_2))     // 3
	fmt.Println(len(intCh_2))     // 1

}


// Закрытие канала
/*
После инициализации канал готов передавать данные. Он находится в открытом состоянии, и мы можем с ним взаимодействовать,
пока не будет закрыт с помощью встроенной функции close():
	intCh := make(chan int, 3)
    intCh <- 10
    intCh <- 3
    close(intCh)
    // intCh <- 24       // ошибка - канал уже закрыт
    fmt.Println(<-intCh)     // 10
    fmt.Println(<-intCh)     // 3
    fmt.Println(<-intCh)     // 0

После закрытия канала мы не сможем послать в него новые данные. Если мы попробуем это сделать, то получим ошибку.
Однако мы можем получить ранее добавленные данные. Но при попытке получить данные из канала, которых нет, мы получим значение по умолчанию.
Например, в примере выше в канал добавляются два значения. При попытке получить третье значение, которого нет в канале, мы получим значение по умолчанию - число 0.

Чтобы не столкнуться с проблемой, когда канал уже закрыт, мы можем проверять состояние канала. В частности, из канала мы можем получить два значения:
val, opened:= <-intCh
Первое значение - val - это собственно данные из канала, а opened представляет логическое значение, которое равно true, если канал открыт
и мы можем успешно считать из него данные. Например, мы можем проверять с помощью условной конструкции состояние канала:

 */

func CloseChannel() {

	intCh := make(chan int, 3)
	intCh <- 10
	intCh <- 3
	close(intCh)    // канал закрыт

	for i := 0; i < cap(intCh); i++ {
		if val, opened := <-intCh; opened {
			fmt.Println(val)
		} else {
			fmt.Println("Channel closed!")
		}
	}
}