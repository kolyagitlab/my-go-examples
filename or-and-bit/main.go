package main

import (
	"log"
	"my-go-examples/or-and-bit/types"
)

func main() {

	want := "JWRPA"

	modeWant := types.ModeUnset

	if err := modeWant.UnmarshalText([]byte(want)); err != nil {
		log.Println("Err in unmarshal: ", err.Error())
	}

	switch modeWant {
	case types.ModeApprove:
		log.Println("Mode is Approve")
	case types.ModePres:
		log.Println("Mode is Approve")
	default:
		log.Println("DEFAULT")
	}

}
