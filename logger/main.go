package main

import (
	"errors"
	"my-go-examples/logger/logger"
	"os"
	"runtime"
)

var globals struct {
	someVarInt int
	someVarStr string
}

func main() {

	logger.Init("logs", 50, 5, 2, false)
	logger.SetLogToConsole(true)
	logger.SetLogFilenameLineNum(true)

	logger.Error("Server run in PID %d; CPU %d process(es)", os.Getpid(), runtime.GOMAXPROCS(runtime.NumCPU()))

	// nil value
	logger.Error("Server run in PID %d; CPU %d process(es)", globals, runtime.GOMAXPROCS(runtime.NumCPU()))

	globals.someVarStr = "abc"

	// this format not support
	logger.Info("aws upload success %s", os.Getpid(), "key", runtime.GOMAXPROCS(runtime.NumCPU()), "id", globals.someVarStr)

	// %s must be STRING, %d --> INT
	logger.Info("aws upload success %s key %s id %d", os.Getpid(), runtime.GOMAXPROCS(runtime.NumCPU()), globals.someVarInt)
	logger.Info("aws upload success %s key %s id %d", os.Getpid(), runtime.GOMAXPROCS(runtime.NumCPU()), globals.someVarStr)

	// success format
	logger.Info("aws upload success %d key %d id %d", os.Getpid(), runtime.GOMAXPROCS(runtime.NumCPU()), globals.someVarInt)

	err := errors.New("some error msg")
	logger.Error("Error with is: %s", err)

	// Without %s not be show err
	logger.Error("Error without is: ", err)

	err = someFuncReturn() // return string
	logger.Error("Error will be: %s", err)
	logger.Error(err.Error())

	err = someFuncReturnNil() // return nil
	logger.Error("Error NIL will be: ", err)
	// panic err is nil
	logger.Error("Error NIL will be %s", err.Error())

}

func someFuncReturn () error {
	return errors.New("some err from function")
}

func someFuncReturnNil () error {
	return nil
}