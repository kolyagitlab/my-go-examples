package main

/*
* 	need go get first
*	go get k8s.io/client-go@v0.17.0
 */

import (
	"flag"
	"fmt"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/tools/clientcmd"
	"os"
	"path/filepath"
	"time"
)

func main() {
	// uses the current context in kubeconfig
	// path-to-kubeconfig -- for example, /root/.kube/config
	//config, _ := clientcmd.BuildConfigFromFlags("", "C:\\Users\\qudrat\\.kube\\config")

	var kubeconfig *string
	if home := homeDir(); home != "" {
		kubeconfig = flag.String("kubeconfig", filepath.Join(home, ".kube", "config"), "(optional) absolute path to the kubeconfig file")
	} else {
		kubeconfig = flag.String("kubeconfig", "", "absolute path to the kubeconfig file")
	}
	flag.Parse()

	// use the current context in kubeconfig
	config, err := clientcmd.BuildConfigFromFlags("", *kubeconfig)
	if err != nil {
		panic(err.Error())
	}

	// create the clientset
	clientset, err := kubernetes.NewForConfig(config)
	if err != nil {
		panic(err.Error())
	}
	for {

		pods, err := clientset.CoreV1().Pods("default").List(metav1.ListOptions{})
		if err != nil {
			panic(err.Error())
		}
		fmt.Printf("There are %d pods in the cluster\n", len(pods.Items))

		_, err = clientset.CoreV1().Pods("default").Get("configmaptestapp-564ff48d9d-5f7x6", metav1.GetOptions{})
		if errors.IsNotFound(err) {
			fmt.Printf("Pod configmaptestapp-564ff48d9d-5f7x6 not found in default namespace\n")
		} else if statusError, isStatus := err.(*errors.StatusError); isStatus {
			fmt.Printf("Error getting pod %v\n", statusError.ErrStatus.Message)
		} else if err != nil {
			panic(err.Error())
		} else {
			fmt.Printf("Found configmaptestapp-564ff48d9d-5f7x6 pod in default namespace\n")
		}


		configMaps, err := clientset.CoreV1().ConfigMaps("default").List(metav1.ListOptions{})
		if err != nil {
			panic(err.Error())
		}

		for _, cm := range configMaps.Items {

			if cm.GetName() == "app-config" {
				//cm.Data = bar2
				cm.Data["foo"] = "bar2"
				_, err := clientset.CoreV1().ConfigMaps("default").Update(&cm)
				if err != nil {
					panic(err.Error())
				}
				fmt.Printf("ConfigMap '%s' updated!\n", cm.GetName())
			}
		}

		time.Sleep(10 * time.Second)
	}

}

func homeDir() string {
	if h := os.Getenv("HOME"); h != "" {
		return h
	}
	return os.Getenv("USERPROFILE") // windows
}