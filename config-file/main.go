package main

import (
	"encoding/json"
	"flag"
	jcr "github.com/DisposaBoy/JsonConfigReader"
	"log"
	"os"
	"path/filepath"
	"runtime"
)

var globals struct {
	someVar bool
}

type validatorConfig struct {
	// TRUE or FALSE to set
	AddToTags bool `json:"add_to_tags"`
	//  Authentication level which triggers this validator: "auth", "anon"... or ""
	Required []string `json:"required"`
	// Validator params passed to validator unchanged.
	Config json.RawMessage `json:"config"`
}

type mediaConfig struct {
	// The name of the handler to use for file uploads.
	UseHandler string `json:"use_handler"`
	// Maximum allowed size of an uploaded file
	MaxFileUploadSize int64 `json:"max_size"`
	// Garbage collection timeout
	GcPeriod int `json:"gc_period"`
	// Number of entries to delete in one pass
	GcBlockSize int `json:"gc_block_size"`
	// Individual handler config params to pass to handlers unchanged.
	Handlers map[string]json.RawMessage `json:"handlers"`
}

type configType struct {
	// Can be overridden from the command line, see option --listen.
	Listen string `json:"listen"`

	CacheControl int    `json:"cache_control"`
	APIKeySalt   []byte `json:"api_key_salt"`

	Cluster json.RawMessage            `json:"cluster_config"`
	Auth    map[string]json.RawMessage `json:"auth_config"`
	Media   *mediaConfig               `json:"media"`
}

func main() {

	executable, _ := os.Executable()

	rootpath, _ := filepath.Split(executable)

	log.Printf("Server run in PID %d; CPU %d process(es)", os.Getpid(), runtime.GOMAXPROCS(runtime.NumCPU()))

	var configfile = flag.String("config", "app.conf", "Path to config file.")
	var listenOn = flag.String("listen", "", "Override address and port to listen on for HTTP(S) clients.")
	flag.Parse()

	*configfile = toAbsolutePath(rootpath, *configfile)
	log.Printf("Using config from '%s'", *configfile)

	var config configType
	if file, err := os.Open(*configfile); err != nil {
		log.Fatal("Failed to read config file: ", err)
	} else if err = json.NewDecoder(jcr.New(file)).Decode(&config); err != nil {
		log.Fatal("Failed to parse config file: ", err)
	}

	if *listenOn != "" {
		config.Listen = *listenOn
	}

	log.Println("App listen on: ", config.Listen)

	// Cluster config
	var configCluster clusterConfig
	if err := json.Unmarshal(config.Cluster, &configCluster); err != nil {
		log.Fatal(err)
	}

	log.Println("Cluster name: ", configCluster.ThisName)

}

type clusterConfig struct {
	// Name of this cluster node
	ThisName string `json:"self"`
}

// Convert relative filepath to absolute.
func toAbsolutePath(base, path string) string {
	if filepath.IsAbs(path) {
		return path
	}
	return filepath.Clean(filepath.Join(base, path))
}
