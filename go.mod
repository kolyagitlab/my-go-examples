module my-go-examples

go 1.14

require (
	github.com/DisposaBoy/JsonConfigReader v0.0.0-20171218180944-5ea4d0ddac55 // indirect
	github.com/cossacklabs/themis/gothemis v0.12.0
	github.com/fsnotify/fsnotify v1.4.9
	github.com/furdarius/rabbitroutine v0.6.0
	github.com/go-redis/redis v6.15.7+incompatible // indirect
	github.com/golang-migrate/migrate/v4 v4.11.0 // indirect
	github.com/golang/glog v0.0.0-20160126235308-23def4e6c14b // indirect
	github.com/golang/protobuf v1.4.1
	github.com/google/uuid v1.1.1
	github.com/gorilla/handlers v1.4.2 // indirect
	github.com/gorilla/websocket v1.4.2 // indirect
	github.com/graarh/golang-socketio v0.0.0-20170510162725-2c44953b9b5f // indirect
	github.com/imdario/mergo v0.3.9 // indirect
	github.com/juggleru/nestedset v0.0.0-20181003123918-6e0f4ff70804
	github.com/kardianos/osext v0.0.0-20190222173326-2bc1f35cddc0
	github.com/kubernetes-client/go v0.0.0-20200222171647-9dac5e4c5400
	github.com/mmcloughlin/geohash v0.10.0
	github.com/pion/logging v0.2.2
	github.com/pion/stun v0.3.3
	github.com/pion/turn v1.4.0
	github.com/pkg/sftp v1.11.0 // indirect
	github.com/prometheus/client_golang v1.6.0
	github.com/prometheus/client_model v0.2.0
	github.com/prometheus/common v0.9.1
	github.com/rubenv/sql-migrate v0.0.0-20200429072036-ae26b214fa43 // indirect
	github.com/streadway/amqp v0.0.0-20200108173154-1c71cc93ed71
	golang.org/x/crypto v0.0.0-20200429183012-4b2356b1ed79 // indirect
	golang.org/x/oauth2 v0.0.0-20200107190931-bf48bf16ab8d // indirect
	golang.org/x/sync v0.0.0-20200317015054-43a5402ce75a
	golang.org/x/time v0.0.0-20200416051211-89c76fbcd5d1 // indirect
	gopkg.in/yaml.v2 v2.2.8
	k8s.io/apimachinery v0.17.0
	k8s.io/client-go v0.17.0
)
