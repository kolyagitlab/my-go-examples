package main

import (
	"fmt"
	"github.com/go-redis/redis"
	"sync"
	"time"
)

// wrap redis options
type Options struct {
	Client *redis.Client
}

type Option func(*Options)

// handle errors
type Err struct {
	Message []byte
	Name    string
	Err     error
}

func (e *Err) Error() string {
	if len(e.Message) > 0 {
		return fmt.Sprintf("%s: |%s| <- `%s`", e.Err, e.Name, string(e.Message))
	}
	return fmt.Sprintf("%s: |%s|", e.Err, e.Name)
}

type RedisQueue struct {
	sendChans    map[string]chan []byte
	receiveChans map[string]chan []byte

	sync.Mutex
	wg sync.WaitGroup

	errChan     chan error
	stopchan    chan struct{}
	stopPubChan chan struct{}
	stopSubChan chan struct{}

	client *redis.Client
}

func NewRedisQueue(opts ...Option) *RedisQueue {
	var options Options
	for _, o := range opts {
		o(&options)
	}

	return &RedisQueue{
		sendChans:    make(map[string]chan []byte),
		receiveChans: make(map[string]chan []byte),
		errChan:      make(chan error, 10),
		stopchan:     make(chan struct{}),
		stopPubChan:  make(chan struct{}),
		stopSubChan:  make(chan struct{}),
		client:       options.Client,
	}
}

func (rq *RedisQueue) newConnection() (*redis.Client, error) {
	var err error
	if rq.client != nil {
		return rq.client, nil
	}

	rq.client = redis.NewClient(&redis.Options{
		Network:    "tcp",
		Addr:       "127.0.0.1:6379",
		Password:   "",
		DB:         0,
		MaxRetries: 0,
	})

	// check conn
	_, err = rq.client.Ping().Result()
	return rq.client, err
}

// Receive gets a channel on which to receive messages with the specified name.
func (rq *RedisQueue) Receive(name string) <-chan []byte {
	rq.Lock()
	defer rq.Unlock()

	ch, ok := rq.receiveChans[name]
	if ok {
		return ch
	}

	ch, err := rq.makeSubscriber(name)
	if err != nil {
		rq.errChan <- &Err{Name: name, Err: err}
		return make(chan []byte)
	}

	rq.receiveChans[name] = ch
	return ch
}

func (rq *RedisQueue) makeSubscriber(name string) (chan []byte, error) {
	c, err := rq.newConnection()
	if err != nil {
		return nil, err
	}

	ch := make(chan []byte, 1024)
	go func() {
		for {
			data, err := c.BRPop(0*time.Second, name).Result()
			if err != nil {
				select {
				case <-rq.stopSubChan:
					return
				default:
					rq.errChan <- &Err{Name: name, Err: err}
					continue
				}
			}

			ch <- []byte(data[len(data)-1])
		}
	}()
	return ch, nil
}

// Send gets a channel on which messages with the specified name may be senrq.
func (rq *RedisQueue) Send(name string) chan<- []byte {
	rq.Lock()
	defer rq.Unlock()

	ch, ok := rq.sendChans[name]
	if ok {
		return ch
	}

	ch, err := rq.makePublisher(name)
	if err != nil {
		rq.errChan <- &Err{Name: name, Err: err}
		return make(chan []byte)
	}
	rq.sendChans[name] = ch
	return ch
}

func (rq *RedisQueue) makePublisher(name string) (chan []byte, error) {
	c, err := rq.newConnection()
	if err != nil {
		return nil, err
	}

	ch := make(chan []byte, 1024)
	rq.wg.Add(1)
	go func() {
		defer rq.wg.Done()
		for {
			select {
			case <-rq.stopPubChan:
				if len(ch) != 0 {
					_, err := rq.client.Ping().Result()
					if err == nil {
						continue
					}
				}
				return
			case msg := <-ch:
				err := c.LPush(name, string(msg)).Err()
				if err != nil {
					rq.errChan <- &Err{Message: msg, Name: name, Err: err}
				}
			}
		}
	}()
	return ch, nil
}

// ErrChan gets the channel on which errors are senrq.
func (rq *RedisQueue) ErrChan() <-chan error {
	return rq.errChan
}

func (rq *RedisQueue) Stop() {
	close(rq.stopSubChan)
	close(rq.stopPubChan)
	rq.wg.Wait()
	_ = rq.client.Close()
	close(rq.stopchan)
}

func (rq *RedisQueue) Done() chan struct{} {
	return rq.stopchan
}

/*
*	Usage
	some_channel1 := transport.Receive("some_channel1")
	some_channel2 := transport.Send("some_channel2")
*/
