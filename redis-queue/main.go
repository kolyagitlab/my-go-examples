package main

import (
	"log"
	"time"
)

var globals struct {
	// Redis queue
	redisQueue *RedisQueue
}

func main() {

	/*
	*	Init Redis queue
	 */
	globals.redisQueue = NewRedisQueue()
	defer func() {
		globals.redisQueue.Stop()
		<-globals.redisQueue.Done()
	}()

	sendMsg([]byte{'h', 'e', 'l', 'l', 'o'})

	go handleConnectionWrite()

	duration := time.Duration(10) * time.Second // Pause for 10 seconds
	time.Sleep(duration)

}

func sendMsg(msg []byte) {

	/*
	*	Init send channel for Redis queue
	 */
	sessionSendQueue := globals.redisQueue.Send("session_send")

	select {
	case sessionSendQueue <- msg:
	}

}

func handleConnectionWrite() {

	// listen connection
	for {
		select {
		case err := <-globals.redisQueue.ErrChan():
			log.Println("an error occurred in redis queue:", err)

		/*
		*	Receive msg from Redis queue channel
		 */

		case msg, ok := <-globals.redisQueue.Receive("session_send"):
			if !ok {
				// Channel closed.
				log.Println("Err: HAS NOT OK!")
				return
			}
			log.Println("Message received: ", msg)
			// write msg to connection
			/// ....
		}
	}

}
