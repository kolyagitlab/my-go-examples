package read_file

import (
	"fmt"
	"github.com/pkg/sftp"
	"golang.org/x/crypto/ssh"
	"log"
	"os"
)

func main() {

	config := &ssh.ClientConfig{
		User:            "dm",
		HostKeyCallback: ssh.InsecureIgnoreHostKey(),
		Auth: []ssh.AuthMethod{
			ssh.Password("web@1234"),
		},
	}

	config.SetDefaults()
	sshConn, err := ssh.Dial("tcp", "192.168.1.121:22", config)
	if err != nil {
		panic(err)
	}
	defer sshConn.Close()
	fmt.Println("Successfully connected to ssh server.")

	// open an SFTP session over an existing ssh connection.
	sftp, err := sftp.NewClient(sshConn)
	if err != nil {
		log.Fatal(err)
	}
	defer sftp.Close()

	srcPath := "/tmp/"
	dstPath := "C:/temp/"
	filename := "tinode.conf"

	// Open the source file
	srcFile, err := sftp.Open(srcPath + filename)
	if err != nil {
		log.Fatal(err)
	}
	defer srcFile.Close()

	// Create the destination file
	dstFile, err := os.Create(dstPath + filename)
	if err != nil {
		log.Fatal(err)
	}
	defer dstFile.Close()

	// Copy the file
	srcFile.WriteTo(dstFile)

}
