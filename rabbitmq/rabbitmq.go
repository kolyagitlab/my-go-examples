package main

import (
	"encoding/json"
	"github.com/streadway/amqp"
	"log"
)

type RabbitMQ struct {
	ch *amqp.Channel
}

type configRabbitMQ struct {
	Host string `json:"host"`
}

func (rabbitMQ *RabbitMQ) Init(rabbitMQConfig json.RawMessage) {

	var config configRabbitMQ
	if err := json.Unmarshal(rabbitMQConfig, &config); err != nil {
		log.Fatalln("RabbitMQ: failed to parse config: ", err)
	}

	conn, err := amqp.Dial(config.Host)
	failOnError(err, "Failed to connect to RabbitMQ")
	defer conn.Close()

	rabbitMQ.ch, err = conn.Channel()
	failOnError(err, "Failed to open a channel")
	defer rabbitMQ.ch.Close()

}

func (rabbitMQ *RabbitMQ) Send(queueName string, msg string) {

	q, err := rabbitMQ.ch.QueueDeclare(
		queueName,
		true,
		false,
		false,
		false,
		nil,
	)
	failOnError(err, "Failed to declare a queue")

	err = rabbitMQ.ch.Confirm(false)
	failOnError(err, "Failed to Confirm a queue")

	pubAck, pubNack := rabbitMQ.ch.NotifyConfirm(make(chan uint64, 1), make(chan uint64, 1))

	body := msg
	msgPublishing := amqp.Publishing{Body: []byte(body)}
	err = rabbitMQ.ch.Publish("", q.Name, true, true, msgPublishing)
	failOnError(err, "Failed to publish a message")

	select {
	case <-pubAck:
		log.Println("-----------------Ack--------------------")
	case <-pubNack:
		log.Println("-----------------NAck-------------------")
	}

}

func (rabbitMQ *RabbitMQ) Receive(queueName string, msg string) {

	q, err := rabbitMQ.ch.QueueDeclare(
		queueName,
		false,
		false,
		false,
		false,
		nil,
	)
	failOnError(err, "Failed to declare a queue")

	msgs, err := rabbitMQ.ch.Consume(
		q.Name, // queue
		"",
		true,
		false,
		false,
		false,
		nil,
	)
	failOnError(err, "Failed to register a consumer")

	forever := make(chan bool)

	go func() {
		for d := range msgs {
			log.Printf("Received a message: %s", d.Body)
		}
	}()

	log.Printf(" [*] Waiting for messages. To exit press CTRL+C")
	<-forever

}

func failOnError(err error, msg string) {
	if err != nil {
		log.Fatalf("%s: %s", msg, err)
	}
}
