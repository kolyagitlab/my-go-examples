package main

import (
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"github.com/streadway/amqp"
	"log"
	"my-go-examples/logger/logger"
	"time"
)

var (
	//uri   = flag.String("uri", "amqp://guest:guest@localhost:5672/", "AMQP URI")
	uri   = flag.String("uri", "amqp://admin:web@1234@localhost:4545/", "AMQP URI")
	queue = flag.String("queue", "queue-iam", "Ephemeral AMQP queue name")
)

func init() {
	flag.Parse()
}

type Rabbit struct {
	Publish    chan []byte // this channel receives socket messages from the message hub
	Deliveries <-chan amqp.Delivery     // this channel listens to rabbitmq deliveries and consumes them
	Done       chan error               // this channel receives a message when there the delivery channel breaks
	Channel    *amqp.Channel            // this is the rabbitmq channel we are using to publish and subscribe to messages
}

var r = Rabbit{
	Publish: make(chan []byte),
	Done:    make(chan error),
}

func dial(amqpURI []byte) (*amqp.Connection, error) {
	log.Printf("rabbit dialing %q", amqpURI)
	connection, err := amqp.Dial(string(amqpURI))
	if err != nil {
		return nil, fmt.Errorf("Dial: %s", err)
	}
	return connection, nil
}

func createChannel(c *amqp.Connection) *amqp.Channel {
	log.Printf("got Connection, getting Channel")
	channel, err := c.Channel()
	if err != nil {
		panic(fmt.Errorf("Channel: %s", err))
	}
	return channel
}

func createQueue(channel *amqp.Channel, queueName string) (*amqp.Queue, error) {
	log.Printf("declared Queue %q", queueName)
	queue, err := channel.QueueDeclare(
		queueName, // name of the queue
		true,      // durable
		false,     // delete when usused
		false,     // exclusive
		false,     // noWait
		nil,       // arguments
	)
	if err != nil {
		return nil, fmt.Errorf("Queue Declare: %s", err)
	}

	log.Printf("declared Queue (%q %d messages, %d consumers)", queue.Name, queue.Messages, queue.Consumers)

	return &queue, nil
}

func getDeliveriesChannel(channel *amqp.Channel, queueName string) (<-chan amqp.Delivery, error) {
	return channel.Consume(
		queueName, // name
		"",        // consumerTag,
		false,     // noAck
		false,     // exclusive
		false,     // noLocal
		false,     // noWait
		nil,       // arguments
	)
}

// 1. init the rabbitmq conneciton
// 2. expect messages from the message hub on the Publish channel
// 3. if the connection is closed, try to restart it

func (r *Rabbit) run() {
	initRabbitConn()
	for {
		select {
		case msg := <-r.Publish:
			fmt.Printf("publish message: %s\n", msg)
			_ = publish(r.Channel, msg)
		case err := <-r.Done:
			log.Println(err)
			initRabbitConn()
		}
	}
}

// try to start a new connection, channel and deliveries channel. if failed, try again in 5 sec.
func initRabbitConn() {
	ticker := time.NewTicker(5 * time.Second)
	quit := make(chan struct{})
	go func() {
		for {
			select {
			case <-ticker.C:
				if conn, err := dial([]byte(*uri)); err != nil {
					log.Println(err)
					log.Println("node will only be able to respond to local connections")
					log.Println("trying to reconnect in 5 seconds...")
				} else {
					close(quit)
					r.Channel = createChannel(conn)
					_, _ = createQueue(r.Channel, *queue)
					//r.Deliveries, _ = getDeliveriesChannel(r.Channel, *queue)
					//go handleConsume(r.Deliveries, r.Done, r.Publish)
				}
			case <-quit:
				ticker.Stop()
				return
			}
		}
	}()
}

func publish(c *amqp.Channel, msg []byte) error {
	body, _ := json.Marshal(msg)
	log.Printf("publishing %dB body (%s)", len(body), body)
	if c == nil {
		return fmt.Errorf("connection to rabbitmq might not be ready yet")
	}
	if err := c.Publish(
		"",
		"queue-iam", // routing to 0 or more queues
		false,             // mandatory
		false,             // immediate
		amqp.Publishing{
			Headers:         amqp.Table{},
			ContentType:     "application/json",
			ContentEncoding: "",
			Body:            msg,
			DeliveryMode:    amqp.Transient, // 1=non-persistent, 2=persistent
			Priority:        0,              // 0-9
			// a bunch of application/implementation-specific fields
		},
	); err != nil {
		return fmt.Errorf("Publish: %s", err)
	}
	return nil
}

func handleConsume(deliveries <-chan amqp.Delivery, done chan error, publish chan []byte) {
	fmt.Println("waiting for rabbitmq deliveries...")
	for d := range deliveries {
		_ = d.Ack(false)
		s := string(d.Body)
		log.Printf(
			"got %dB delivery: [%v] %s",
			len(s),
			d.DeliveryTag,
			s,
		)

		// publish to queue
		publish <- []byte{'H', 'E', 'L'}
	}
	done <- fmt.Errorf("error: deliveries channel closed")
}

func main() {
	flag.Parse()
	go r.run() // rabbitmq pubsub listener
	r.PublishToReportService("fffffff")

}

func (r *Rabbit) PublishToReportService(msg interface{}) error {
	logger.Info("Publish data to Report Service queue")
	if r == nil {
		errMsg := "RabbitMQ instance not initialized"
		logger.Error(errMsg)
		return errors.New(errMsg)
	}
	r.Publish  <- serialize(msg)
	return nil
}

func serialize(msg interface{}) []byte {
	out, _ := json.Marshal(msg)
	return out
}