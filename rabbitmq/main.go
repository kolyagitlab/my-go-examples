package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"time"

	"github.com/google/uuid"

	"github.com/streadway/amqp"
)

var uri = flag.String("uri", "amqp://admin:web@1234@192.168.1.128:5672/", "AMQP URI")
var exchange = flag.String("exchange", "exchange-1", "AMQP EXCHANGE")
var queue = flag.String("queue", "sub-1", "AMQP QUEUE")
var bindingKey = flag.String("bindingKey", "xxx", "AMQP BINDING KEY FOR QUEUE")

func init() {
	flag.Parse()
}

func logError(err error) {
	log.Printf("Error: %v", err)
	os.Exit(1)
}

func main() {

	log.Printf("dialing %v", *uri)
	connection, err := amqp.Dial(*uri)
	if err != nil {
		logError(err)
	}
	defer connection.Close()

	log.Printf("getting channel")
	channel, err := connection.Channel()
	if err != nil {
		logError(err)
	}

	err = channel.Confirm(false)
	if err != nil {
		panic(err)
	}

	pubAck, pubNack := channel.NotifyConfirm(make(chan uint64, 1), make(chan uint64, 1))

	log.Printf("declaring exchange: %v", *exchange)
	err = channel.ExchangeDeclare(*exchange, "direct", true, false, false, false, nil)
	if err != nil {
		logError(err)
	}

	log.Printf("declaring queue: %v", *queue)
	_, err = channel.QueueDeclare(*queue, true, false, false, false, nil)
	if err != nil {
		logError(err)
	}

	err = channel.QueueBind(*queue, *bindingKey, *exchange, false, nil)
	if err != nil {
		logError(err)
	}

	stopPublisher := make(chan bool, 1)
	stopConsumer := make(chan bool, 1)

	log.Println("Press ENTER to stop the publisher")
	log.Println("Press ENTER again to stop the consumer")
	log.Println("Starting process in 3 seconds")
	time.Sleep(3 * time.Second)

	go func() {
		for {
			select {
			case <-stopPublisher:
				return
			default:

				uid := uuid.New()
				err = channel.Publish(*exchange, *bindingKey, false, false, amqp.Publishing{
					Headers:         amqp.Table{},
					ContentType:     "text/plain",
					ContentEncoding: "",
					Body:            []byte(uid.String()),
					DeliveryMode:    amqp.Persistent,
					Priority:        0,
				})
				if err != nil {
					logError(err)
				} else {
					//log.Println("Published message ", uid.String())
				}

			}
		}
	}()

	go func() {
		deliveries, err := channel.Consume(*queue, "some-tag", false, false, false, false, nil)
		if err != nil {
			logError(err)
		}

		for {
			select {
			case <-stopConsumer:
				return
			case d := <-deliveries:
				b := string(d.Body)
				log.Println("Consumed message ", b)
				err = d.Ack(false)
				if err != nil {
					logError(err)
				}
			}
		}
	}()

	select {
	case <-pubAck:
		fmt.Println("Ack")
	case <-pubNack:
		fmt.Println("NAck")
	}

	var input string
	_, _ = fmt.Scanln(&input)
	stopPublisher <- true
	log.Printf("stopping publisher...")
	time.Sleep(1 * time.Second)
	_, _ = fmt.Scanln(&input)
	stopConsumer <- true
	log.Printf("stopping consumer...")
	time.Sleep(1 * time.Second)
}
