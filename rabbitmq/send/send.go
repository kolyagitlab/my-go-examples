package main

import (
	"encoding/base64"
	"encoding/json"
	"github.com/streadway/amqp"
	"log"
)

func failOnError(err error, msg string) {
	if err != nil {
		log.Fatalf("%s: %s", msg, err)
	}
}

type ServerComMessage struct {
	Id     string
	Rcptto string
}

type MsgClientAcc struct {
	Id string `json:"id,omitempty"`
	// "new" to create a new user or "usrBFh498hfdf" to update a user; default: current user.
	User string `json:"user,omitempty"`
	// Authentication level of the user when UserID is set and not equal to the current user. Either "", "auth" or "anon". Default: ""
	AuthLevel string
	// Authentication token for resetting the password and maybe other one-time actions.
	Token []byte `json:"token,omitempty"`
	// The initial authentication scheme the account can use
	Scheme string `json:"scheme,omitempty"`
	// Shared secret
	Secret []byte `json:"secret,omitempty"`
	// Authenticate session with the newly created account
	Login bool `json:"login,omitempty"`
	// Indexable tags for user discovery
	Tags []string `json:"tags,omitempty"`
	// User initialization data when creating a new user, otherwise ignored
	Desc *MsgSetDesc `json:"desc,omitempty"`
	// Credentials to verify (email or phone or captcha)
	Cred []MsgCredClient `json:"cred,omitempty"`
}
type MsgSetDesc struct {
	DefaultAcs *MsgDefaultAcsMode `json:"defacs,omitempty"` // default access mode
	Public     interface{}        `json:"public,omitempty"`
	Private    interface{}        `json:"private,omitempty"` // Per-subscription private data
}
type MsgDefaultAcsMode struct {
	Auth string `json:"auth,omitempty"`
	Anon string `json:"anon,omitempty"`
}
type MsgCredClient struct {
	// Credential type, i.e. `email` or `tel`.
	Method string `json:"meth,omitempty"`
	// Value to verify, i.e. `user@example.com` or `+18003287448`
	Value string `json:"val,omitempty"`
	// Verification response
	Response string `json:"resp,omitempty"`
	// Request parameters, such as preferences. Passed to valiator without interpretation.
	Params interface{} `json:"params,omitempty"`
}

func main() {
	conn, err := amqp.Dial("amqp://admin:web@1234@192.168.1.128:5672/")
	failOnError(err, "Failed to connect to RabbitMQ")
	defer conn.Close()

	ch, err := conn.Channel()
	failOnError(err, "Failed to open a channel")
	defer ch.Close()

	secret, err := base64.URLEncoding.DecodeString("cXVkcmF0OndlYkAxMjM0") // base64.encode("qudrat:web@1234") //cXVkcmF0OndlYkAxMjM0

	m := MsgClientAcc{Id: "1", User: "new", Scheme: "basic", Secret: secret, Login: true, Desc: &MsgSetDesc{Public: "{}"},
		Cred: []MsgCredClient{{Method: "email", Value: "alimov.kolya@gmail.com"}},
	}

	body := serialize(m)
	err = ch.Publish(
		"",          // exchange
		"queue-iam", // routing key
		false,       // mandatory
		false,       // immediate
		amqp.Publishing{
			Headers:         amqp.Table{},
			ContentType:     "application/json",
			ContentEncoding: "",
			Body:            body,
			DeliveryMode:    amqp.Persistent,
			Priority:        0,
		})
	log.Printf(" [x] Sent %s", body)
	failOnError(err, "Failed to publish a message")
}

func serialize(msg MsgClientAcc) []byte {
	out, _ := json.Marshal(msg)
	return out
}

/*func main() {
	conn, err := amqp.Dial("amqp://admin:web@1234@192.168.1.128:5672/")
	failOnError(err, "Failed to connect to RabbitMQ")
	defer conn.Close()

	ch, err := conn.Channel()
	failOnError(err, "Failed to open a channel")
	defer ch.Close()

	err = ch.Confirm(false)
	if err != nil {
		panic(err)
	}
	pubAck, pubNack := ch.NotifyConfirm(make(chan uint64, 1), make(chan uint64, 1))
	fmt.Println("Publish to queue: ", "admin_console_queue")
	msg := amqp.Publishing{
		Body: []byte("Hello")}
	err = ch.Publish("", "admin_console_queue3", true, true, msg)
	if err != nil {
		panic(err)
	}
	select {
	case <-pubAck:
		fmt.Println("Ack")
	case <-pubNack:
		fmt.Println("NAck")
	}
}*/
