package main

import (
	"github.com/streadway/amqp"
	"io"
	"log"
	"sync"
	"time"
)

const (
	name = "queue-iam"
	addr = "amqp://admin:web@1234@192.168.1.128:5672/"
)

// RabbitMQ ...
type RabbitMQ struct {
	Logger      *log.Logger
	IsConnected bool
	addr        string
	name        string
	connection  *amqp.Connection
	channel     *amqp.Channel
	queue       *amqp.Queue
	consume     <-chan amqp.Delivery
	wg          *sync.WaitGroup
	done        chan bool
}

const retryDelay = 5 * time.Second

// NewQueue creates a new queue instance.
func NewQueue(logOut io.Writer, name string, addr string) *RabbitMQ {
	rabbit := RabbitMQ{
		IsConnected: false,
		addr:        addr,
		name:        name,
		wg:          new(sync.WaitGroup),
		done:        make(chan bool),
		Logger:      log.New(logOut, "", log.LstdFlags),
	}
	rabbit.wg.Add(1)
	rabbit.Connect()
	go rabbit.reconnect()
	return &rabbit
}

// reconnect waits to be notified about a connection
// error, and then attempts to reconnect to RabbitMQ.
func (rabbit *RabbitMQ) reconnect() {
	defer rabbit.wg.Done()
	graceful := make(chan *amqp.Error)
	errs := rabbit.channel.NotifyClose(graceful)
	for {
		select {
		case <-rabbit.done:
			return
		case <-graceful:
			graceful = make(chan *amqp.Error)
			rabbit.Logger.Println("Graceful close!")
			rabbit.IsConnected = false
			rabbit.Connect()
			rabbit.IsConnected = true
			errs = rabbit.channel.NotifyClose(graceful)
		case <-errs:
			graceful = make(chan *amqp.Error)
			rabbit.Logger.Println("Normal close")
			rabbit.IsConnected = false
			rabbit.Connect()
			errs = rabbit.channel.NotifyClose(graceful)
		}
	}
}

// Connect will block until a new connection to
// RabbitMQ is formed.
func (rabbit *RabbitMQ) Connect() {
	for {
		conn, err := amqp.Dial(rabbit.addr)
		if err != nil {
			rabbit.Logger.Println("Failed to establish connection")
			time.Sleep(retryDelay)
			continue
		}
		ch, err := conn.Channel()
		if err != nil {
			rabbit.Logger.Println("Failed to create a channel")
			time.Sleep(retryDelay)
			continue
		}
		queue, err := ch.QueueDeclare(
			name,
			true, // Durable
			false, // Delete when unused
			false, // Exclusive
			false, // No-wait
			nil,   // Arguments
		)
		if err != nil {
			rabbit.Logger.Println("Failed to publish a queue")
			time.Sleep(retryDelay)
			continue
		}
		rabbit.Logger.Println("Connected!")
		rabbit.IsConnected = true
		rabbit.connection = conn
		rabbit.channel = ch
		rabbit.queue = &queue
		return
	}
}

// Close the connection to RabbitMQ and stop
// checking for reconnections.
func (rabbit *RabbitMQ) Close() error {
	close(rabbit.done)
	rabbit.wg.Wait()
	return rabbit.connection.Close()
}

//func main() {
//	fmt.Println("Starting...")
//	NewQueue(os.Stdout, name, addr)
//	select {}
//}
