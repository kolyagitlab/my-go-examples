package main

import (
	"fmt"
	"math/rand"
	"time"
)


// async-await

/*
// Javascript.

const longRunningTask = async () => {
    // Simulate a workload.
    sleep(3000)
    return Math.floor(Math.random() * Math.floor(100))
}

const r = await longRunningTask()
console.log(r)

 */

func longRunningTask() <-chan int32 {
	r := make(chan int32)

	go func() {
		defer close(r)

		// Simulate a workload.
		time.Sleep(time.Second * 3)
		r <- rand.Int31n(100)
	}()

	return r
}

func main() {
	r := <-longRunningTask()
	fmt.Println(r)
}




// promise All

/*
// Javascript.

const longRunningTaskPromise = async () => {
// Simulate a workload.
sleep(3000)
return Math.floor(Math.random() * Math.floor(100))
}

const [a, b, c] = await Promise.all(longRunningTask(), longRunningTask(), longRunningTask())
console.log(a, b, c)

 */

func longRunningTaskPromise() <-chan int32 {
	r := make(chan int32)

	go func() {
		defer close(r)

		// Simulate a workload.
		time.Sleep(time.Second * 3)
		r <- rand.Int31n(100)
	}()

	return r
}

func main2() {
	aCh, bCh, cCh := longRunningTaskPromise(), longRunningTaskPromise(), longRunningTaskPromise()
	a, b, c := <-aCh, <-bCh, <-cCh

	fmt.Println(a, b, c)
}