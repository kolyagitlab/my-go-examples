var SocketioClient = {
    createNew: function (host, port) {
        var client = {};
        client.host = host;
        client.port = port;
        client.socket = io('ws://' + host + ':' + port, {transports: ['websocket']})
        client.socket.on('connect', () => {
            console.log('CLIENT CONNECTED. socketID:' + client.socket.id);
        });
        client.socket.on('connect_error', (error) => {
            console.error("connect_error: " + error);
        });
        client.socket.on('connect_timeout', (timeout) => {
            console.log("connect_timeout: " + timeout);
        });
        client.socket.on('error', (error) => {
            console.log("error: " + error);
        });
        client.socket.on('disconnect', (reason) => {
            console.log("disconnect. Reason: " + reason);
        });
        client.socket.on('reconnect', (attemptNumber) => {
            console.log("reconnect. AttemptNumber: " + attemptNumber);
        });
        client.socket.on('reconnect_error', (error) => {
            // ...
        });
        client.socket.on('reconnect_failed', () => {
            // ...
        });
        client.socket.on('ping', () => {
        });
        client.socket.on('pong', (latency) => {
        });
        client.send = function (event, msg, callback) {
            if (callback) {
                client.socket.emit(event, msg, (data) => callback);
            } else {
                client.socket.emit(event, msg);
            }
        }
        client.receive = function (event, callback) {
            client.socket.on(event, callback);
        }
        return client;
    }
}