package main

import (
	"github.com/graarh/golang-socketio/transport"
	"log"
	"net/http"

	"github.com/graarh/golang-socketio"
)

/*
* 	OTHER PROJECT SocketIO as nodejs socketIO
* 	https://github.com/jjeffcaii/socket.io
 */

func main() {
	//create
	server := gosocketio.NewServer(transport.GetDefaultWebsocketTransport())

	//handle connected
	server.On(gosocketio.OnConnection, func(c *gosocketio.Channel) {
		log.Println("New client connected: ", c.Id())
		//join them to room
		c.Join("chat")
	})

	type Message struct {
		Name    string `json:"name"`
		Message string `json:"message"`
	}

	//handle custom event
	server.On("from-client", func(c *gosocketio.Channel, msg Message) string {
		//send event to all in room
		channel, _ := server.GetChannel("client id here")
		//and send the event to the client
		//type MyEventData struct {
		//	Data: string
		//}
		_ = channel.Emit("SEND_BY_CLIENT_SOCKET_ID", "sjdflkjdsflksdgfkls")

		return "OK"
	})

	//you can get client connection by it's id

	//setup http server
	serveMux := http.NewServeMux()
	serveMux.Handle("/socket.io/", server)
	log.Panic(http.ListenAndServe(":8000", serveMux))
}
