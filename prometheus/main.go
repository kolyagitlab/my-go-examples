package main

import (
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/prometheus/common/log"
	"my-go-examples/prometheus/other"
	"my-go-examples/prometheus/simple"
	"net/http"
)

func main() {

	// Start some prometheus

	simple.Main()

	other.AllTogether()

	other.ExampleCounter()
	other.ExampleCounterVec()
	other.ExampleGauge()
	other.ExampleGaugeVec()
	//other.ExampleGaugeFunc()
	//other.ExampleHistogram()
	//other.ExampleNewConstHistogram()
	//other.ExampleSummary()
	//other.ExampleSummaryVec()
	//other.ExampleNewConstSummary()
	//other.ExampleAlreadyRegisteredError()
	other.ExampleGatherers()
	//other.ExampleNewMetricWithTimestamp()

	//This section will start the HTTP server and expose
	//any metrics on the /metrics endpoint.
	http.Handle("/metrics", promhttp.Handler())
	log.Info("Beginning to serve on port :6222")
	log.Fatal(http.ListenAndServe(":6222", nil))

}
