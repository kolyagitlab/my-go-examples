package simple

import (
	"github.com/prometheus/client_golang/prometheus"
)

func Main() {

	//Create a new instance of the foocollector and
	//register it with the prometheus client.
	foo := newFooCollector()
	prometheus.MustRegister(foo)

}
