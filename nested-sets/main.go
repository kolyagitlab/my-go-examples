package main

import (
	"fmt"
	"github.com/juggleru/nestedset"
	"strings"
)

type MySomeType struct {
	*nestedset.Node // add it to your any type

	// type vars
	MyId int64
	MyName string
}

// Init it in instance creation
func NewMySomeType() *MySomeType {
	return &MySomeType{
		Node: nestedset.NewNode(),
	}
}

// You can redefine NodeInterface functions

// Return your type
func (t *MySomeType) Type() string {
	return "mysometype"
}

// Return your inner id
func (t *MySomeType) Id() int64 {
	return t.MyId
}

// Return your inner name
func (t *MySomeType) Name() string {
	return t.MyName
}

// Set your inner id or generate it
func (t *MySomeType) SetId(id int64) {
	t.MyId = id // or t.MyId = getSomeNewId()
}

// Set your inner name
func (t *MySomeType) SetName(name string) {
	t.MyName = name
}

func main() {

	ns := nestedset.NewNestedSet()

	// create 3 new nodes
	node1 := NewMySomeType()
	node1.MyName = "Node 1"
	node2 := NewMySomeType()
	node2.MyName = "Node 2"
	node3 := NewMySomeType()
	node3.MyName = "Node 3"

	ns.Add(node1, nil)   // add node to root
	ns.Add(node2, nil)   // add node to root
	ns.Add(node3, node1) // add node to node1

	ns.Move(node3, node2) // move node3 from node1 to node2

	branch := ns.Branch(nil) // get full tree

	// print tree with indents
	for _, n := range branch {
		fmt.Print(strings.Repeat("..", int(n.Level())))
		fmt.Printf("%s Level:%d, Left:%d, Right:%d\n", n.Name(), n.Level(), n.Left(), n.Right())
	}
}

/*
	https://github.com/sugchand/Go-NestedSet    it's with REST API example
*/