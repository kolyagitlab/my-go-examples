package main

import "log"

type Range struct {
	Low int
	Hi  int
}

func main() {

	numbers := [10]int{1, 4, 5, 6, 8, 9, 13, 14, 15, 19}
	//numbers := []int{14}
	//numbers := []int{1, 2, 3, 6, 8, 9, 13, 15, 16, 17}
	var filterRanges []Range

	for i := 0; i < len(numbers); i++ {
		var rstart = numbers[i]
		var rend = rstart
		for i < len(numbers)-1 && numbers[i+1]-numbers[i] == 1 {
			rend = numbers[i+1]
			i += 1
		}
		if rstart == rend {
			filterRanges = append(filterRanges, Range{
				Low: rstart,
				Hi:  rstart,
			})
		} else {
			filterRanges = append(filterRanges, Range{
				Low: rstart,
				Hi:  rend,
			})
		}
	}

	log.Printf("RESULT: %v", filterRanges)


}
