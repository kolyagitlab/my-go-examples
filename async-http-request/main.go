package main

import (
	"fmt"
	"log"
	"net/http"
	"regexp"
)

type HttpResp struct {
	Resp *http.Response
	Err  error
}

func asyncGet(url string) <-chan *HttpResp {
	r := make(chan *HttpResp)

	go func(u string) {
		defer close(r)

		resp, err := http.Get(u)
		r <- &HttpResp{resp, err}

	}(url)

	return r
}

func main() {

	r := <-asyncGet("http://127.0.0.1:6060/restart")
	if r.Err != nil {
		log.Println("Error: ", r.Err)
	} else {
		fmt.Println("Good: ", r.Resp.StatusCode)
	}


	//var strs []string
	//for i := 0; i < 100; i++ {
	//	strs = append(strs, "aaa_"+strconv.Itoa(i))
	//}
	//for _, str := range strs {
	//	r := <-asyncGet("http://127.0.0.1:5060/start?pod=" + str + "&ip=1.1.1.2")
	//	if r.Err != nil {
	//		log.Println("Error: ", r.Err)
	//	} else {
	//		fmt.Println("Good: ", r.Resp.StatusCode)
	//	}
	//}

}

func checkContainsStrinStr(){
	const (
		str    = "Get \"http://10.16.0.55:18080/restart\": dial tcp 10.16.0.55:18080: connect: connection refused"
		substr = "connection refused"
	)
	log.Println(regexp.MatchString(substr, str))
}